***Settings***

Documentation       Arquivo Base De Automação

Library             Browser
Library             Collections
Library             DateTime
Library             OperatingSystem
Library             libs/DeloreanLibrary.py

Resource            actions/auth.robot
Resource            actions/nav.robot
Resource            actions/students.robot
Resource            actions/components.robot
Resource            actions/plans.robot
Resource            actions/enrollments.robot

***Keywords***
Start Browser Session
    New Browser     ${browser}        ${headless}
    New Page        About:Blank
Start Admin Session
    Start Browser Session
    Login Page
    Login With  admin@bodytest.com  pwd123
    User Should Be Logged In  Administrador
Clear LS and Print
    Take Screenshot
    LocalStorage Clear
Delayed Print
    [Arguments]     ${timeout}
    Sleep           ${timeout}
    Take Screenshot

#Helpers
Get JSON 
    [Arguments]     ${file_name}

    ${file}     Get File    ${EXECDIR}/resources/fixtures/${file_name}
    ${jsonobject}       Evaluate        json.loads($file)      json

    [Return]    ${jsonobject}  

