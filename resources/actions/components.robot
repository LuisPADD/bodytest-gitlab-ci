***Settings***
Documentation   Açoes de componentes genéricos

***Keywords***
##Validations
Toaster Should Be
    [Arguments]     ${expected_text}
    Wait For Elements State     css=.Toastify__toast-body >> text=${expected_text}  visible     5
    
Span Should Be
    [Arguments]     ${expected_span}
    Wait For Elements State     css=form span >> text=${expected_span}              visible     5

  
Field Should Be Type
    [Arguments]     ${element}  ${type}
    ${attr}             Get Attribute   ${element}  type
    Should Be Equal     ${attr}         ${type}

Register Should Not be Found
    Wait for Elements State   css=div >> text=Nenhum registro encontrado.   visible    5
    Wait for Elements State   css=table      detached        5

Total Items Should Be
    [Arguments]     ${number}

    ${element}                   Set Variable                css=#pagination .total
    Wait for Elements State      ${element}                   visible               5
    Get Text                     ${element}     ==            Total: ${number}

Email Should Not Visible
    [Arguments]     ${email}
    Get Element States   xpath=//td[contains(text( ), "${email}")]        validate    hidden

Email Should Visible
    [Arguments]     ${email}
    Get Element States   xpath=//td[contains(text( ), "${email}")]        validate    visible

Name Should Be Visible 
    [Arguments]                  ${name}
    Wait for Elements State      css=table tbody tr >> text=${name}       visible   5
Name Not Should Be Visible 
    [Arguments]                  ${name}
    Wait for Elements State      css=table tbody tr >> text=${name}       hidden    5


#Return Elements & Text
Get Required Alerts 
    [Arguments]     ${index}
    ${span}     Get Text    xpath=(//form//span)[${index}]
    [Return]    ${span}
#Buttons
Confirm Removal
    Click           xpath=//button[contains(text( ), "SIM, pode apagar!")]

Cancel Removal
    Click           xpath=//button[contains(text( ), "N")]
