***Settings***
Documentation   Ações de Auth

***Keywords***
Login Page
    Go to       https://btwepluis.fly.dev/

Login With  
    [Arguments]     ${email}    ${senha}

    Fill Text       css=input[name=email]            ${email}
    Fill Text       css=input[placeholder*=senha]    ${senha}
    Click           text=Entrar

