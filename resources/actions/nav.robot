***Settings***
Documentation   Ações do menu de navegação superior

***Keywords***
User Should Be Logged In 
    [Arguments]     ${username}
    Get Text    css=aside strong      should be      ${username}

Go to Students
    Click       css=a[href$=alunos] >> text = ALUNOS               Left
    Wait For Elements State     css=h1 >> text=Gestão de Alunos     visible     5
Go To Plans
    Click       css=a[href$=planos] >> text = PLANOS               Left
    Wait For Elements State     css=h1 >> text=Gestão de Planos     visible     5
Go To Enrollments
    Click       css=a[href$=matriculas] >> text = MATRÍCULAS               Left
    Wait For Elements State     css=h1 >> text=Gestão de Matrículas     visible     5

