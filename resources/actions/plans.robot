***Settings***

Documentation       Ações de feature de gestão de planos

***Variables***
${TITLE_FIELD}            id=title
${DURATION_FIELD}         id=duration
${PRICE_FIELD}            css=input[name=price]
${TOTAL_FIELD}            css=input[name=total]

***Keywords***
#Forms
Go To Form Plan
    Click       css=a[href$="planos/new"]
    Wait For Elements State     css=h1 >> text=Novo plano           visible     5

Fill Form Plan
    [Arguments]     ${plan} 

    Fill Text       ${TITLE_FIELD}           ${plan.title}
    Fill Text       ${DURATION_FIELD}        ${plan.duration}
    Fill Text       ${PRICE_FIELD}           ${plan.price}

Search Plan by Title
    [Arguments]     ${title}
    Fill Text       css=input[placeholder="Buscar plano"]   ${title}

Update Plan
    [Arguments]     ${plan} 

    Fill Text       ${TITLE_FIELD}           ${plan['title']}
    Fill Text       ${DURATION_FIELD}        ${plan['duration']}
    Fill Text       ${PRICE_FIELD}           ${plan['price']}

    Submit Plans Form


#Buttons
Go To Plan Update By Title and Duration
    [Arguments]         ${title}        ${duration}
    Click                       xpath=//td[contains(text(),"${title}")]/..//td[contains(text(), ${duration})]/..//a[@class="edit"]
    Wait for Elements State     css= h1 >> text="Edição de plano"       visible         5
Request Plan Removal by Title
    [Arguments]     ${title}
    Click           xpath=//td[contains(text( ), "${title}")]/../td//button[@id="trash"]

Submit Plans Form

    Click           xpath=//button[contains(text( ), "Salvar")]

#Validations
Total Plan Should Be    
    [Arguments]        ${total}

    Get Attribute       ${TOTAL_FIELD}  value   ==  ${total}

Check Type Field on Plans Form
    [Arguments]               ${element}    ${type}

    Go to Plans
    Go To Form Plan
    Field Should Be Type      ${element}    ${type}

Title Should not Be Visible 
    [Arguments]                  ${title}
    Wait for Elements State      css=table tbody tr >> text=${title}       hidden   5

Title Should Be Visible 
    [Arguments]                  ${title}
    Wait for Elements State      css=table tbody tr >> text=${title}       visible   5

