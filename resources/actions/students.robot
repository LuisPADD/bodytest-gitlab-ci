***Settings***

Documentation       Ações de feature de gestão de alunos

***Variables***
${NAME_FIELD}           css=input[name=name]
${EMAIL_FIELD}          css=input[name=email]
${AGE_FIELD}            css=input[name=age]
${WEIGHT_FIELD}         css=input[name=weight]
${FEET_TALL_FIELD}      css=input[name=feet_tall]

***Keywords***
    ##Forms
New Student
    [Arguments]     ${student} 

    Fill Text       ${NAME_FIELD}           ${student.name}
    Fill Text       ${EMAIL_FIELD}          ${student.email}
    Fill Text       ${AGE_FIELD}            ${student.age}
    Fill Text       ${WEIGHT_FIELD}         ${student.weight}
    Fill Text       ${FEET_TALL_FIELD}      ${student.feet_tall}

    Submit Student Form


Check Type Field on Student Form
    [Arguments]               ${element}    ${type}

    Go to Students
    Go To Form Student
    Field Should Be Type      ${element}    ${type}

Search Student by name
    [Arguments]     ${name}
    Fill Text       css=input[placeholder="Buscar aluno"]   ${name}

Update Student
    [Arguments]     ${student} 

    Fill Text       ${NAME_FIELD}           ${student['name']}
    Fill Text       ${EMAIL_FIELD}          ${student['email']}
    Fill Text       ${AGE_FIELD}            ${student['age']}
    Fill Text       ${WEIGHT_FIELD}         ${student['weight']}
    Fill Text       ${FEET_TALL_FIELD}      ${student['feet_tall']}

    Submit Student Form


    ##buttons
Go To Form Student
    Click       css=a[href$="alunos/new"]
    Wait For Elements State     css=h1 >> text=Novo aluno           visible     5

Submit Student Form

    Click           xpath=//button[contains(text( ), "Salvar")]

Request Student Removal by Email
    [Arguments]     ${email}
    Click           xpath=//td[contains(text( ), "${email}")]/../td//button[@id="trash"]

Go To Student Form Update By Email
    [Arguments]         ${email}
    Click  //td[contains(text(),"${email}")]/..//a[@class="edit"]
    Wait for Elements State     css= h1 >> text="Edição de aluno"       visible         5