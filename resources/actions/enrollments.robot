***Settings***

Documentation       Ações de feature de gestão de matriculas

***Keywords***
#forms
Select Student
    [Arguments]     ${name}
    Fill text       css=input[aria-label=student_id]    ${name}
    Click           css=div[id*=option] >> text=${name}

Select Plan
    [Arguments]     ${title}
    Fill text       css=input[aria-label=plan_id]    ${title}
    Click           css=div[id*=option] >> text=${title}

Submit Enroll Form
    Click                                  css=button[type=submit]

#Links and Buttons
Go To Form Enrollments
    Click       css=a[href$="matriculas/new"]
    Wait For Elements State     css=h1 >> text=Nova matrícula           visible     5

#Validations
Today Date Should Be
    ${start}        Get Current Date            result_format=%d/%m/%Y
    Get Attribute     css=input[name=start_date]      Value     ==         ${start}

End Date Should Be
    [Arguments]     ${days}
    ${start}        Get Current Date            
    ${end}          Add Time to Date            ${start}        ${days} days     result_format=%d/%m/%Y
    Get Attribute     css=input[name=end_date]        Value     ==         ${end}  

