***Settings***

Documentation          Suite de teste de login do admin

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Browser Session
Test Teardown          Take Screenshot

***Test Cases***
Cenario: Login do Administrador
    [Tags]      login
    Login Page
    Login With                 admin@bodytest.com  pwd123
    User Should Be Logged In   Administrador

    [Teardown]          Clear LS and Print
    
Cenario: Senha Incorreta
    [Tags]      senhaincorreta
    Login Page
    Login With         admin@bodytest.com  abc123
    Toaster Should Be  Usuário e/ou senha inválidos.

    [Teardown]          Delayed Print  1

Cenario: Email Não Cadastrado
    [Tags]      senhaincorreta
    Login Page
    Login With         Fernando@gmail.com  abc123
    Toaster Should Be  Usuário e/ou senha inválidos.

    [Teardown]          Delayed Print  1
Cenario: E-Mail Incorreto
    [Tags]      emailincorreto
    Login Page
    Login With      admin&bodytest.com  pwd123
    Span Should Be  Informe um e-mail válido

Cenario: Senha Não informada
    [Tags]      senhanaoinformada
    Login Page
    Login With      admin@bodytest.com  ${EMPTY}
    Span Should Be  A senha é obrigatória

Cenario: Email Não Informado
    [Tags]      emailnaoinformada
    Login Page
    Login With      ${EMPTY}    pwd123
    Span Should Be  O e-mail é obrigatório

Cenario: Email e Senha Não Informado
    [Tags]      emailsenha
    Login Page
    Login With      ${EMPTY}  ${EMPTY}
    Span Should Be  O e-mail é obrigatório
    Span Should Be  A senha é obrigatória 



    

