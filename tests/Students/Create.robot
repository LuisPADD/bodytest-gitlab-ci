***Settings***

Documentation       Cadastros de novos Alunos

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Admin Session
Test Teardown          Delayed Print    1


***Test Cases***
Cenario: Novo Aluno
    &{student}          Create Dictionary           name=Olavo de Carvalho  email=olavocaveira@gmail.com    age=92  weight=60   feet_tall=1.60

    Remove Student      ${student.email}
    #Pre-Condição
    Go to Students
    Go To Form Student

    New Student         ${student}
    #Validação
    Toaster Should Be   Aluno cadastrado com Sucesso.
    [Teardown]          Delayed Print  1

Cenario: Email Duplicado
    [tags]      dup
    &{student}          Create Dictionary           name=Maradona  email=maradona@gmail.com    age=90  weight=72   feet_tall=1.75

    Insert Student      ${student}
   
    Go to Students
    Go To Form Student
    New Student         ${student}
    #Validação
    Toaster Should Be   Email já existe no sistema.
    [Teardown]          Delayed Print  1

Cenario: Todos os campos tem que ser preenchidos
    [Tags]      temp

    @{expected_alerts}      Set Variable       Nome é obrigatório  O e-mail é obrigatório  idade é obrigatória    o peso é obrigatório    a Altura é obrigatória
    @{got_alerts}           Create List

    Go to Students
    Go To Form Student
    Submit Student Form

    FOR     ${index}    IN RANGE    1   6
            ${span}     Get Required Alerts      ${index}
            Append To List  ${got_alerts}        ${span}
    END

    Lists Should be Equal    ${got_alerts}   ${expected_alerts}

Cenario: Validar Tipo Numero
    [Tags]  number
    [Template]  Check Type Field on Student Form
    ${AGE_FIELD}         number
    ${WEIGHT_FIELD}      number
    ${FEET_TALL_FIELD}   number

Cenario: Validar Tipo Email
    [Tags]  email
    [Template]  Check Type Field on Student Form    
    ${EMAIL_FIELD}       email

Cenario: Menor de 14 Não Pode Fazer Cadastros
    [Tags]      menor
    &{student}                      Create Dictionary   name=Carles Henriquepedia  email=carles@gmail.com   age=13  weight=60   feet_tall=1.65

    Go to Students
    Go To Form Student
    New Student                     ${student}

    Span Should Be                  A idade deve ser maior ou igual 14 anos











    


