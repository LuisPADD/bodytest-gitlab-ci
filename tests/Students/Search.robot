***Settings***

Documentation          Buscar Alunos

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Admin Session
Test Teardown          Delayed Print    1

***Test Cases***

Cenario: Busca Exata

    &{student}          Create Dictionary           name=Steve Jobs     email=stevejobs@apple.com   age=92  weight=60   feet_tall=1.60

    Remove Student By Name       ${student.name}
    Insert student               ${student}
    Go to Students
    Search Student by name       ${student.name}
    Name Should Be Visible       ${student.name}
    Total Items Should Be        1

Cenario: Registro Não Encontrado
    [Tags]          temp
    ${name}         Set Variable    Amoedo Junior

    Go to Students
    Remove Student By Name   ${name}
    Search Student by name   ${name}
    Register Should Not be Found

Cenario: Busca Alunos por um Unico Termo
    [Tags]              json
    
    ${fixture}       Get JSON             students-search.json
    ${students}      Set Variable         ${fixture['students']}

    ${word}          Set Variable         ${fixture['word']}
    ${total}         Set Variable         ${fixture['total']}
    
    Remove Student by Name                ${word}

    FOR     ${item}   IN  @{students}
        Insert Student                    ${item}
    END    

    Go to Students
    Search Student by name                ${word}

    FOR     ${item}   IN  @{students}
        Name Should Be Visible            ${item['name']}
    END    

    Total Items Should Be                 ${total}









