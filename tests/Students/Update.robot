***Settings***

Documentation          Atualizar Cadastro de Alunos

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Admin Session
Test Teardown          Delayed Print    1

***Test Cases***
Cenario: Atualizar Aluno Ja Cadastrado
    #Antes
    ${fixture}  Get JSON  students-update.json

    ${capeta}   Set Variable                ${fixture['Before']}
    ${diabo}    Set Variable                ${fixture['After']}

    Remove Student by Name                  ${capeta['name']}
    Remove Student by Name                  ${diabo['name']}

    Insert Student                          ${capeta}
    Go to Students
    Search Student by name                  ${capeta['name']}
    Go To Student Form Update By Email      ${capeta['email']}
    Update Student                          ${diabo}
    Toaster Should Be                       Aluno Atualizado com sucesso.

    


