***Settings***

Documentation       Remoçao de Alunos Cadastrados

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Admin Session


***Test Cases***
Cenario: Remove um Estudante Cadastrado

    &{student}                      Create Dictionary   name=Carles Henriquepedia  email=carles@gmail.com   age=13  weight=60   feet_tall=1.65

    Insert student                      ${student}
    Go to Students
    Search Student by name              ${student.name}
    Request Student Removal by Email    ${student.email}
    Confirm Removal
    Toaster Should Be                   Aluno removido com sucesso.
    Email Should Not visible            ${student.email}

Cenario: Desistir da Exclusão
    [Tags]      desis
    &{student}                          Create Dictionary   name=Charles Henriquepedia  email=charles@gmail.com   age=13  weight=60   feet_tall=1.65

    Insert student                      ${student}
    Go to Students
    Search Student by name              ${student.name}
    Request Student Removal by Email    ${student.email}
    Cancel Removal
    Email Should Visible                ${student.email}










