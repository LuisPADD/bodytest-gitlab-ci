***Settings***

Documentation          Gestao de Matriculas

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Admin Session
Test Teardown          Delayed Print    1

***Test Cases***
Cenario: Deve Exibir a data inicial e final Conforme o Plano Escolhido
                 

    ${fixture}       Get JSON              enroll-dates.json
    ${diabo}         Set Variable          ${fixture['student']}
    ${infern}        Set Variable          ${fixture['plans']}

    Remove Student by Name                 ${diabo['name']}

    Insert Student                         ${diabo}
    Insert Plan                            ${infern}

    Go To Enrollments
    Go To Form Enrollments
    Select Student  ${diabo['name']}
    Select Plan     ${infern['title']}      
    Today Date Should Be
    End Date Should Be      ${fixture['days']}
      
Cenario: Deve matricular um Aluno em um Plano
    [Tags]          mat
    ${fixture}      Get JSON             enroll-create.json
    ${saci}         Set Variable          ${fixture['student']}
    ${mesob}        Set Variable          ${fixture['plans']}

    Remove Student by Name                 ${saci['name']}
    Insert Student                         ${saci}
    Insert Plan                            ${mesob}

    Go To Enrollments
    Go To Form Enrollments

    Select Student                         ${saci['name']}        
    Select Plan                            ${mesob['title']}
    Submit Enroll Form
    Toaster Should Be                      Matrícula cadastrada com sucesso              



