***Settings***

Documentation          Cadastros de Planos

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Admin Session
Test Teardown          Delayed Print    1

***Test Cases***
Cenario: Calcular Preço Total do Plano
    &{plan}     Create Dictionary       title=Parneas    duration=12     price=19,99     total=R$\xa0239,88
    Go To Plans
    Go To Form Plan

    Fill Form Plan                      ${plan}
    Total Plan Should Be                ${plan.total}

Cenario: Criar Plano
    &{plan}     Create Dictionary       title=Pazorpes    duration=12     price=25,50
    Go To Plans
    Go To Form Plan
    Fill Form Plan                      ${plan}
    Submit Plans Form

    Toaster Should Be                   Plano cadastrado com Sucesso

Cenario: Todos os campos tem que ser preenchidos
    [Tags]      temp

    @{expected_alerts}      Set Variable       Informe o título do plano  Informe a duração do plano em meses
    @{got_alerts}           Create List

    Go To Plans
    Go To Form Plan
    Submit Plans Form

    FOR     ${index}    IN RANGE    1   3
            ${span}     Get Required Alerts      ${index}
            Append To List  ${got_alerts}        ${span}
    END

    Lists Should be Equal    ${got_alerts}   ${expected_alerts}

Cenario: Validar Tipo De Campo
    [Tags]  number
    [Template]  Check Type Field on Plans Form
    ${DURATION_FIELD}         number
    ${PRICE_FIELD}            text


