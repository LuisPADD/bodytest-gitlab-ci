***Settings***

Documentation          Buscar Planos

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Admin Session
Test Teardown          Delayed Print    1

***Test Cases***

Cenario: Busca Exata

    &{plan}     Create Dictionary       title=Pindorfa    duration=12     price=19.99     total=R$\xa0239,88

    Remove Plan                  ${plan.title}
    Insert Plan                  ${plan}
    Go to Plans
    Search Plan by Title         ${plan.title}
    Title Should Be Visible      ${plan.title}
    Total Items Should Be        1

Cenario: Registro Não Encontrado
    [Tags]          temp
    ${title}        Set Variable    Parideos

    Go to Plans
    Remove Plan              ${title}
    Search Plan by Title     ${title}
    Register Should Not be Found

Cenario: Busca Planos por um Unico Termo
    [Tags]              json
    
    ${fixture}       Get JSON             plans-search.json
    ${plan}         Set Variable          ${fixture['plans']}

    ${word}          Set Variable         ${fixture['word']}
    ${total}         Set Variable         ${fixture['total']}
    
    Remove All Plans                           ${word}

    FOR     ${item}   IN  @{plan}
        Insert Plan                       ${item}
    END    

    Go to Plans
    Search Plan by Title                  ${word}

    FOR     ${item}   IN  @{plan}
        Title Should Be Visible           ${item['title']}
    END    

    Total Items Should Be                 ${total}









