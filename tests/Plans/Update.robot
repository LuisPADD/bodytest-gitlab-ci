***Settings***

Documentation          Atualizar Planos

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Admin Session
Test Teardown          Delayed Print    1

***Test Cases***
Cenario: Atualizar Plano Ja Existente
    #Antes
    ${fixture}     Get JSON                    plans-update.json

    ${semestral}   Set Variable                ${fixture['Before']}
    ${anual}       Set Variable                ${fixture['After']}

    Remove All Plans                           ${semestral['title']}
    Remove All Plans                           ${anual['title']}

    Insert Plan                                ${semestral}
    Go to Plans
    Search Plan by Title                       ${semestral['title']}
    Go To Plan Update By Title and Duration    ${semestral['title']}    ${semestral['duration']}
    Update Plan                                ${anual}
    Toaster Should Be                          Plano Atualizado com sucesso


    


