***Settings***

Documentation          Remoçao de Planos Cadastrados

Resource               ${EXECDIR}/resources/base.robot

Suite Setup            Start Admin Session
Test Teardown          Delayed Print  1


***Test Cases***
Cenario: Remove um Plano Cadastrado

    &{plan}     Create Dictionary       title=Patneas    duration=12     price=19.99     total=R$\xa0239,88

    Insert plan                         ${plan}
    Go to Plans
    Search Plan by Title                ${plan.title}
    Request Plan Removal by Title       ${plan.title}
    Confirm Removal
    Toaster Should Be                   Plano removido com sucesso
    Title Should not Be Visible         ${plan.title}

Cenario: Desistir da Exclusão
    [Tags]      desis
    &{plan}     Create Dictionary       title=Pindorfa    duration=12     price=19.99     total=R$\xa0239,88

    Insert plan                         ${plan}
    Go to Plans
    Search Plan by Title                ${plan.title}
    Request Plan Removal by Title       ${plan.title}
    Cancel Removal
    Title Should Be Visible             ${plan.title}

